## A propos de moi

![](images/avatar-photo.jpg)

Je m'appelle **Léa Debauche**, j'ai 21 ans et je suis en première année de master à la faculté d'architecture de Bruxelles.

Cette année j'ai choisi l'option architecture et design pour dévelloper ma créativité et élargir mes connaissances dans ce domaine.

## Mon parcours

Je viens d'un petit village situé dans la province de Namur, où j'ai suivi une formation scolaire générale et profité de mon temps libre pour découvrir diverses activités **sportives**, comme le tennis, l'équitation et l'escalade, mais aussi des activités **artistiques**, j'ai suivi des cours de piano et obtenu mon diplôme de solfège.

Plusieurs membres de ma famille sont des **artisants**,
j'ai toujours aimé le **travail à la main** et le fait d'être capable de réaliser les choses par sois même. Depuis aussi longtemps ue je m'en souvienne, lorsque j'ai besoin d'une chose je cherche avant tout une façon de le réaliser par mes propres moyens.

J'ai toujours été très **curieuse et désireuse d'apprendre dans tous les domaines**.
C'est ce qui m'a guidé vers l'architecture et aujourd'hui vers le design.

## Projet

Dans le cadre du cours d'option, il nous est demandé de choisir un objet proche de nous sur lequel on désire travailler.

L'objet que j'ai choisi m'accompagne depuis quelques années dans mes études et dans ma vie courante, il s'agit de mon **tube à dessin**.

j'ai toujours aimé pouvoir travailler à la main,
ne venant pas de bruxelles cet objet m'a permis de faire de nombreux voyages avec mes travaux pour pouvoir travailler n'importe où et également me permettre de partager mon travail avec mes proches.

C'est un objet que j'aimerais avoir tout le temps sur moi mais **sa taille devient vite encombrante** surtout quand on doit le transporter vide après avoir déposé des documents par exemple, ou simplement pour transporter des formats plus petits.

De nombreux étudiants reconnaissent la grande utilité de cet objet mais l'abandonne rapidement à cause de son coté encombrant. Je désire donc travailler sur le tube à dessin pour le rendre plus **modulable** et pratique en fonction de tous les moments de son utilisation.

![This is another caption](images/sample-photo.jpg)
