# Module 3. Impression 3D

A travers ce module vous pouvez suivre la formation d'**impressions d’objet 3D** sur les imprimantes de type Prusa.

Il nous a été demandé de réaliser notre objet ou une partie de celui-ci. L’élément que j’ai choisi est le **couvercle du tube à dessin** car c’est un élément que je pouvais réaliser assez rapidement et entièrement, sans réduire son échelle.

Vous pouvez retrouver le modèle fusion utilisé en suivant le lien ci dessous :

[couvercle du tube_fusion](https://a360.co/3CV4eA4)

[couvercle du tube_STL](../images/couvercle tube .stl)

## Telecharger prusa slicer

Avant toutes choses, il est nécessaire d’installer prusaSlicer sur son ordinateur en suivant le lien ci-dessous

[PrusaSlicer_download](http://www.prusa3d.com/drivers/)

Et télécharger les versions suivantes, qui correspondent à celles utilisées au fablab :

**ORIGINAL PRUSA I3 MK3S & MK3S+**

**ORIGINAL PRUSA I3 MK3**

![](../images/mod3_photo_1.jpg)

## Importer un fichier dans PrusaSlicer

Rendez vous désormais dans fusion 360, et sélectionnez le fichier avec l’objet que vous souhaitez imprimer.

Enregister la dernière version de l’objet et exportez ensuite celui-ci en fichier de type « STL ».

![](../images/mod3_photo_2.jpg)

Ensuite rendez-vous dans PrusaSlicer, dans fichier, importer STL.

Votre objet est désormais visible sur la plaque fictive de l’imprimante 3D Prusa.

![](../images/mod3_photo_3.jpg)

Si vous appuyez sur « découper maintenant » le programme va calculer le temps et le coût de réalisation de votre objet.

En ce qui me concerne j’obtiens un résultat de 11 heures de temps de réalisation ce qui est beaucoup trop important, on me conseille de réduire ce temps à un maximum de deux heures.

Pour réduire le temps de production il faut se rendre dans les réglages d’impressions, Dans la partie « couche et périmètre » je choisi une épaisseur de 2 mm pour toutes les couches de mon objet (de manière a ce qu’il soir uniforme).

Dans la partie « remplissage » Je choisi une densité de remplissage de 15% (cela ne sert à rien d’aller au-delà de 25%) et un remplissage en grille.

Ayant un objet avec une grande surface de contacte avec la plaque de chauffe, je n’ai pas besoin d’ajouter de bordure, cet objet n’a pas non plus de partie en porte à faux donc pas besoin de supports.

![](../images/mod3_photo_4.jpg)

Pour réduire considérablement le temps de réalisation je modifie les réglage d’impression et je choisi une précision de 0,2 mm Speed.

![](../images/mod3_photo_5.jpg)

Une fois tous mes critères modifiés j’appuie sur « découper maintenant » pour voir le résultat. J’obtient un temps de réalisation raisonnable de 1h47m.

![](../images/mod3_photo_6.jpg)

Une fois que tout vous convient appuyez sur « Exporter le G-code » et enregistrez votre fichier sur une carte SD en créant un dossier à votre nom.

## Réaliser l’objet sur l’imprimante 3D

 Il est important de nettoyer votre espace de travail, à l’aide du produit et de papier mis à disposition, nettoyez la plaque de l’imprimante 3D pour ôter toutes substances qui pourraient empecher la matière d’accrocher au plateau.

![](../images/mod3_photo_7.jpg)

Pour introduire votre fichier, Inserez la carte SD dans sur le coté gauche de l’imprimante 3D et sélectionnez le fichier à votre nom.

Une fois que l’imprimante est à bonne température l’impression commence, restez présent le temps des trois premères couches pour vérifier que l’impression se déroule comme prévu.

Après minimum 30 minutes d’impression je peux augmenter la vitesse de l’imprimante à 150% pour gagner du temps supplémentaire.

![](../images/mod3_photo_8.jpg)

Une fois l’impression terminée, je laisse refroidir le plateau un instant puis je l’enlève de son support aimanté.

Le plateau est légèrement souple ce qui vous permet de décoller votre objet sans problème.

Je nettoie la plaque avant de la replacer sur l’imprimante.

Mon objet est terminé  !

![](../images/mod3_photo_9.jpg)


**problème rencontré**

Malheureusement la partie supérieur sur laquelle se trouve le filetage est trop fine, après quelque temps elle s’est désolidarisée du reste de l’objet.

**solution**

J’ai modifié le fichier fusion en élargissant de 2 mm le périmètre du couvercle vers l’extérieur pour épaissir le contour et régler le problème.

voici le résultat final !

![](../images/mod3_photo_10.jpg)
