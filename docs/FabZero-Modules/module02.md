# Module 2. Conception Assistée par Ordinateur

Dans le cadre du deuxième module, nous allons apprendre à concevoir l'objet de notre choix sur le programme **fusion 360**.
Pour rappel l'objet que j'ai choisi de représenter est un tube à dessin.

A la fin de ce module voici ce que vous serez en mesure de réaliser, Vous pouvez retrouver le modèle fusion en suivant le lien ci dessous :

[tube à dessin_Fusion](https://a360.co/3nWZ4gJ)

[tube à dessin_STL](../images/tube à dessin.stl)

Ce programme fait partie de la suite autodesk et est disponible avec une licence étudiant gratuite d'une durée d'un an ou une version d'essais de 30 jours, pour le télécharger rendez vous sur le lien suivant :

[Fusion_download](https://www.autodesk.be/fr/products/fusion-360/overview)

## Réflexions

Avant de me lancer dans la conception de l'objet j'ai pris le temps de le décomposer en 3 parties distinctes :

- Le tube fixe
- Le tube coulissant
- Le couvercle

## Partie 1 Tube fixe

Rendez vous dans le programme fusion 360, avant toutes choses, Je me place toujours sur l’origine pour commencer mon esquisse.

Je commence par dessiner le profil de la première partie de mon tube à dessin à l’aide de l’outil ligne. Je prends bien soin de noter chaque dimension. un moyen simple de savoir si votre esquisse est bien réalisée est de vérifier que toutes les lignes tracées apparaissent bien en noir et non en bleu.

Une fois le profil terminé j’appuie sur "terminer l’esquisse" pour passer au travail en 3 dimensions.

![](../images/objet_image_1.jpg)

Pour transformer ce profil en tube je sélectionne mon profil et l’outil "révolution".

Je sélectionne l’axe vert, il s'agit de l’axe autour duquel mon profil va effactuer une rotation de 360 degrés pour former un cylindre.

J’utilise ensuite l’outil "congé" qui me sert à arrondir la base de mon tube sur une hauteur de 5 millimètres.

![](../images/objet_image_2.jpg)

Je créer ensuite une deuxième esquisse pour pouvoir dessiner les 3 boutons présent à l’intérieur du tube (ils permettent de régler et de bloquer la partie coulissante du tube qui se glisse à l’intérieur).

Au centre du tube, je dessine trois cercles de 10 millimètres de diamètre, chacun distant entre eux de 10 millimètres également.

J’appuie sur "terminer l’esquisse" pour accéder au modèle 3D.

![](../images/objet_image_3.jpg)

Je sélectionne mes trois cercles et j’utilise l’outil "extrusion" pour former les trois boutons de 5 millimètre de hauteur.

Pour les dissocier de l’objet principal j’utilise l’outil "scinder le corps" et je sélectionne mes trois boutons.

Ensuite je sélectionne les trois cercle formés sur la face extérieure du tube et j’utilise l’outil "coque" pour leur donner leur forme creuse, je réalise une coque d’un millimètre d’épaisseur.

Pour ne plus voir les traces de séparations je resélectionne l’ensemble et j’utilise l’outil "combiner".

![](../images/objet_image_4.jpg)

A l’intérieur du tube je sélectionne l’extrémité de mes trois boutons et j’utilise l’outil "congé" pour leur donner leur forme arrondie.

![](../images/objet_image_5.jpg)

Je retourne dans la partie esquisse pour finaliser l’attache du tube.

Je dessine un rectangle de 30 millimètres de long sur la largeur de la bande d’attache.

Ensuite, une fois l'esquisse terminée, grâce à l’outil "extrusion" je viens créer un nouveau volume de 8 millimètre de hauteur.

Pour créer le trou dans lequel vient se glisser la lanière j’utilise l’outil "coque" et je sélectionne les deux faces "ouvertes" ensuite je crée une coque d’une épaisseur de 2 millimètres tout autour.

Pour que l’ensemble de l’attache soit plus harmonieux j’utilise l’outil "combiner" et je sélectionne l’ensemble des parties que je souhaite assembler.

![](../images/objet_image_6.jpg)

La première partie du tube est terminée il est temps de passer à la partie qui viendra coulisser à l’intérieur.

## Partie 2 Tube coulissant

Pour ne pas être dérangée dans ma nouvelle esquisse je déplace au préalable la première partie que je viens de réaliser plus loin dans la zone de dessin.

Comme pour la partie 1 je commence une esquisse et dessine le profil de mon objet depuis l’origine.

Je viens ensuite réaliser l'objet en 3D grâce à l’outil "révolution" autour de l'axe vert j’effectue une rotation de 360 degrés pour former le volume plein.

Cette partie de tube n’a pas de fond je viens corriger cela grâce à l’outil "extrusion" dans opération je choisi "couper" et j’enlève l’épaisseur exacte du fond du tube (1 millimètre).

![](../images/objet_image_8.jpg)

Le sommet du tube possède une partie où l’on peut visser le capuchon, pour réaliser cela je sélectionne l’outil "filetage" et la surface à l’extrémité du tube.
Une fenêtre s'ouvre, l'outil me propose un dimensionnement par défault mais  je peux décocher le dimensionnement automatique et régler tous les paramètres du filetage en me basant sur les dimension réelle de mon tube.

![](../images/objet_image_9.jpg)

**problèmes rencontrés**

Mon filetage apparait en grisé et pas en 3D ou il crée un trou dans mon objet.

![](../images/objet_image_13.jpg)

**solutions**

j'ai simplement oublié de cocher la case "modélisé", étape très importante car elle va concevoir votre filtage en 3D directement sur votre objet, si cette étape n'est pas appliquée le filtage n'apparaitra pas au moment de l'impression 3D.

Si après modélisation, le filtage cause des perforations dans le modèle, c'est sûrement parce que votre surface n'est pas assez épaisse, pour régler le problème modifiez vos paramètres avec un filtage plus fin ou modifiez l'esquisse et augmentez l'épaisseur de la surface à traiter.

![](../images/objet_image_14.jpg)

## Partie 3 couvercle

Je recommence une nouvelle esquisse cette fois je dessine le dernier élément de mon tube, je commence par un cercle de 90 millimètre de diamètre.

Une fois l'esquisse terminée je viens extruder mon cercle de 35 mm.

![](../images/objet_image_10.jpg)

Ensuite je viens me placer sous le volume grâce à l'outil "orbite" situé en bas de l'écran.
Je sélectionne la face plane visible et j’utilise l’outil "coque" pour créer un couvercle de 2 mm d’épaisseur.

Je viens ensuite sélectionner la surface courbe à l’intérieur du couvercle et j’utilise l’outil "filetage, avec les mêmes réglages que vu précédement.

Je termine en sélectionnant le bord externe du couvercle et grâce à l’outil "congé" je peux arrondir le bord sur 5 millimètres.

![](../images/objet_image_11.jpg)

L’objet est terminé voici ces éléments présentés séparéments et assemblés.

![](../images/objet_image_15.jpg)
