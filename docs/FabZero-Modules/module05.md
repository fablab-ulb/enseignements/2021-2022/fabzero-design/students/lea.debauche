# Module 5. Usinage assisté par ordinateur

Cette semaine, nous avons assisté à une dernière formation sur la machine shaper origin.
C’est une machine semblable à la défonceuse CNC, toutefois grâce à son coté mobile (direction assistée), elle permet de concevoir des découpes ou gravures sur des formats plus grand et à un prix plus abordable.

Dans ce module vous aurez une présentation et un mode d’emplois de tout le matériel nécessaire à la bonne utilisation de la shaper origin.

Ensuite vous suivrez pas à pas la marche à suivre pour la réalisation d’un objet sur une plaque en bois.

### Informations importantes

Avant utilisation prenez connaissance de ces informations, elles vous guideront dans ce qui est réalisable ou non et vous empêcheront également de prendre des risques inutiles lors de l’utilisation de la machine.

**Spécificité de la shaper origin :**

-	Profondeur de découpe max. : 43 mm
-	Diamètre du collet : 8 mm ou 1/8"
-	Format de fichier : SVG

**Précautions d’utilisation :**

-	Utiliser la machine sur un plan de travail stable
-	Fixer le matériau sur le plan de travail
-	Relever la fraiseuse (bouton rouge) avant de passer d'une forme à une autre
-	Utiliser un aspirateur de poussières

Pour plus d’informations spécifiques, référez-vous aux liens suivant :

[fablab machines](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Shaper.md?fbclid=IwAR3YfQHFz7t0VP8Em3iT0C5DJ2s4WE2_OKRHSKgsMNZBmE3THjI83OetA2g)

[Manuel shaper origin](https://assets.shapertools.com/manual/Shaper_Origin_Product_Manual.pdf)

[shaper site officiel](https://www.shapertools.com/fr-fr/origin/spec)

## Présentation du matériel

Dans les grandes lignes, la machine est dotée de deux poignées de directions, d’un écran tactile coté utilisateur et d’une caméra à l’arrière. Elle mesure 349.3 x 196.9 mm avec une hauteur de 298.5 mm pour un poids total de 6,6 kg.

![](../images/mod5_image1.jpg)

2.Shaper tape (rouleau de 45 m) permet de couvrir une surface de plus ou moins 25 x 12,5 m
3.Clé T hexagonale (4 mm) pour le retrait de la broche
4.Trois sortes de fraises
     - Fraise hélicoïdale 6 mm
     - Fraise hélicoïdale 3 mm
     - Fraise à graver (angle de pointe 60°)
5.Clé (19 mm) pour le collet
6.Tuyau pour les aspirateurs de 2 m de long (Buse de 57 cm de diamètre)

**Retrait et installation du mandrin :**

Dans un premier temps, détacher le mandrin de la machine ensuite desserrer grâce à la clé T et soulever le mandrin.

![](../images/mod5_image2.jpg)

**Changer la mèche :**

Grâce à la clé desserrer pour enlever la mèche actuelle ensuite placer la nouvelle et serrer délicatement (pas besoin de serrer trop fort vous risquez d’abimer l’appareil).

**ATTENTION**

Les mèches sont très fragiles toujours bien les remettre dans leur boitier si elle tombe elles risquent de se casser.

![](../images/mod5_image3.jpg)

**Utilisation du shaper tape :**

Il s’agit de bandes autocollantes sur lesquelles sont imprimé une série de domino, c’est bande serviront de repère à la machine pour évaluer la surface d’utilisation que vous souhaitez.

**ATTENTION**

Ces bandes doivent être placées uniquement sur la plaque que vous souhaitez découper veillez à ce qu’il n’y ai pas de bandes parasites dans le champs de vision de la caméra car cela fausserait ses calculs.

Pour un résultat optimal placez au minimum 3 bandes à 8 cm d’intervalle chacune, de la façon suivante :

![](../images/mod5_image4.jpg)

## Réalisation de la découpe

-	Fixer la planche que vous voulez découper avec du double face sur votre zone de travail pour éviter qu’elle ne bouge pendant la découpe. Placer votre planche sur une surface que vous pouvez abimer en cas de percement trop profond.

-	Une fois votre planche stable et fixe placer votre shaper tape sur autant de surface que nécessaire.

-	Brancher l’aspirateur à la machine (placer l’aspirateur de telle façon à ce qu’il ne vous gêne pas pendant l’opération de découpe) et brancher également le câble d’alimentation de la shaper origin.

-	Scanner la zone de travail, pour cette étape faite glisser la machine sur toute la surface avec la caméra toujours en direction du shaper tape pour ne pas qu’elle perde ses repère, une fois l’ensemble de la surface scanner ramener la à sa position initiale.

-	Importer votre fichier SVG comprenant le dessin à découper via une clé USB que vous pouvez brancher directement sur la machine.

![](../images/mod5_image5.jpg)

Pour vous aider à choisir vos paramètres de fraisage, voici un lien qui vous aidera à les définir avec précision selon le résultat souhaité :

[shaper site officiel](https://support.shapertools.com/hc/fr-fr/articles/115002721473-Concevoir-pour-Origin)

Aperçu

![](../images/mod5_image6.jpg)

Si vous souhaitez graver encoder un épaisseur de fraisage plus petite que l’épaisseur de votre planche. Pour une découpe encoder la valeur exacte de son épaisseur.

Une fois tous vos paramètre défini, allumé l’aspirateur et démarrer la shaper origin en pressant le Bouton vert (à droite).

C’est parti ! suivez le guidage de la machine en cas d’erreur pas de panique la fraise se relève automatiquement, si vous voulez faire une pause appuyez sur le bouton orange (à gauche).

Pour reprenez là ou vous en étiez en pressant à nouveau le bouton vert.

## Résultat

La première partie n’a pas été découpée, la profondeur choisir dans les paramètre était plus petite que la profondeur de la planche le reste à bien été découpé après rectification de ce paramètre.

![](../images/mod5_image7.jpg)
