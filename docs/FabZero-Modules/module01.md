# Module 1. GIT

Dans ce module, vous allez avant tout suivre **l'installation** et la configuration de Git.

Vous y trouverez également la marche à suivre pour **cloner** une copie locale (sur votre ordinateur) de votre Git grâce à une clé Ssh.

Pour terminer, une façon de **documenter** votre site depuis votre copie locale vous sera expliquée par le biais du programme ATOM .

Voici également un lien pratique vers un guide du language Markdown pour vous aider à mettre en page votre documentation : [Guide_Markdown](https://www.ionos.fr/digitalguide/sites-internet/developpement-web/markdown/)  

### Informations importantes

Système d’exploitation utilisé : Microsoft Windows

Outils de travail :

- Terminal de l'ordinateur (accessible depuis la barre de recherche)

- Git Bash   [download](https://gitforwindows.org/)

- ATOM   [download](http://https://atom.io/)

- GIMP [download](https://www.gimp.org/downloads/)  

## 1. Installation de Git

La première chose à faire est de se rendre dans son terminal et de vérifier si Git est déjà installé sur son ordinateur. Utilisez la commande suivante :

```
Microsoft Windows [version 10.0.19042.1237]
(c) Microsoft Corporation. Tous droits réservés.

C:\Users\leade>git --version
git version 2.33.0.windows.2
```
Dans mon cas il est installé, on peut voir que la dernière version est présente.
Si ce n'est pas le cas suivez le lien ci dessous :

[Instal_Git](https://docs.gitlab.com/ee/topics/git/how_to_install_git/index.html)

### Configuration de Git

Ensuite je m'identifie grâce à un nom d’utilisateur et une adresse électronique.

```
C:\Users\leade>git config --global user.name "lea_debauche"
C:\Users\leade>git config --global user.email "lea.debauche@ulb.be"
```
La commande qui suit me permet de visualiser si les données ont bien été prises en compte.
Si vous souhaitez modifier une des données encodées il suffit de recommencer la manipulation ci-dessus.

```
C:\Users\leade>git config --global --list
user.name=lea_debauche
user.email=lea.debauche@ulb.be
```

## 2. Créer une copie locale de Git grâce à une clé Ssh

Git est un système permettant de travailler localement pour ensuite partager les modifications sur GitLab. Pour communiquer ces informations en toute sécurité on utilise le système de clés SSH comme moyen d’identification.

### Créer une clé SSH

Avant de créer une clé SSH, je vérifie si il en existe déjà une.

```
C:\Users\leade>.ssh/
'.ssh' n'est pas reconnu en tant que commande interne
ou externe, un programme exécutable ou un fichier de commandes.
```
Dans mon cas il n’en existe pas, je vais donc en générer une sur base de l’algorithme suivant : ED25519.

J'ai ajouté mon adresse électronique comme commentaire mais ce n’est pas obligatoire.

```
C:\Users\leade>ssh-keygen -t ed25519 -C "lea.debauche@ulb.be"
Generating public/private ed25519 key pair.
Enter file in which to save the key (C:\Users\leade/.ssh/id_ed25519):
```
J'appuie ensuite sur ENTER pour sauvegarder la clé.

### Lier la clé au compte Gitlab

il est tant de générer la clé Ssh

**Problème rencontré**

Apres plusieurs tentatives, la ligne de commande suivante qui permet de générer la clé n’est pas reconnue comme commande interne.
```
C:\Users\leade>cat ~/.ssh/id_ed25519.pub
'cat' n'est pas reconnu en tant que commande interne
ou externe, un programme exécutable ou un fichier de commandes.
```
**solution**

Il est nécessaire d'utiliser un autre outil de travail, Git Bash.
[Git Bash_download](https://gitforwindows.org/)

ce système est identique au terminal de votre ordinateur mais reconnaitra d'avantage de commandes interne. Cependant il possède un gros désavantage car il ne permet pas d’effectuer de copier-coller de texte, il faut réécrire manuellement en incluant toutes les subtilités ! attention aux espaces !

Une fois dans Git Bash entrer la commande suivante :

```
leade@DESKTOP-4M0TKVI MINGW64 ~
$ cat ~/.ssh/id_ed25519.pub
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAIciryGpGHk6LrezLn8yaJj5VhjloV+cd2eYORCDzN4 lea.debauche@ulb.be
```
la clé est enfin générée , il faut maintenant l'ajouter à son compte Gitlab.
Je me rends sur mon compte Gitlab en ligne :
- user settings
- ssh key

1. Je copie-colle la clé dans la zone indiquée
2. Je confirme l'ajout de cette clé

![](../images/sample-pic-2.jpeg)

### cloner avec la clé SSH

Désormais il s'agit de venir cloner ces dossiers sur notre ordinateur grâce à la clé ssh.
Pour se faire je me rend sur mon profil Gitlab à l'endroit suivant :

![](../images/clone.jpg)


J'ouvre mon terminal et j'indique la commande "git clone" suivi du lien que je récupère dans la partie "clone with ssh".

```
C:\Users\leade> git clone git@gitlab.com:fablab-ulb/enseignements/2021-2022/fabzero-design/students/lea.debauche.git
Cloning into 'lea.debauche'...
The authenticity of host 'gitlab.com (172.65.251.78)' can't be established
ED25519 key fingerprint is SHA256:eUXGGm1YGsMAS7vkcx6JOJdoGHPem5gQp4taiCfCLB8.
This key is not known ny any other names
Are you sure you want to continue connecting (yes/no/fingerprint)?
```
J'écris "yes" pour confirmer.

```
Are you sure you want to continue connecting (yes/no/fingerprint)? yes
Warning: Permanently addes 'gitlab.com' (ED25519) to the list of known hosts.
```
J'indique ensuite mon mot de passe, si vous n'en avez pas appuyez simplement sur ENTER.

```
Enter passphrase for key '/C:\Users\leade/.ssh/id_ed25519':
remote: Enumerating objects: 42, done.
remote: Total 42 (delta 0), reused 0 (delta 0), pack-reused 42
Receiving objects: 100% (42/42), 206.55 KIB | 804.00 KIB/s, done.
Resolving deltas: 100% (19/19), done
```
Pour l'étape suivante je retourne dans Git Bash pour vérifier l'emplacements de mes fichiers clonés.

- Je tape "cd" suivi de l'endroit ou vous avez cloner le fichier pour ouvrir sont emplacement.

- Je tape "ls" pour voir le contenu de ce fichier fichier.

```
leade@DESKTOP-4M0TKVI MINGW64 ~
$ cd lea.debauche

leade@DESKTOP-4M0TKVI MINGW64 ~/lea.debauche (main)
$ ls
README.md  docs/  mkdocs.yml  requirements.txt
```
vérification OK !

## 4. Documenter son site depuis la copie locale avec ATOM

Comment documenter sur son site ?

Il existe deux moyens :

-	Le premier se trouve directement sur le site du gitlab, vous pouvez modifier séparément chaque fichier en cliquant sur l’onglet « édit » présent dans ceux-ci.

-	Le second se trouve en local sur votre ordinateur grâce au clonage des fichiers que vous venez d’effectuer.

Pour travailler depuis votre ordinateur en local, il est conseillé de télécharger ATOM qui vous fournira un espace de travail en adéquation avec l’exercice demandé.

Télécharger le grâce au lien suivant :  [ATOM_download](http://https://atom.io/)

Une fois téléchargé vous pourrez ouvrir les fichiers sur lesquels vous désirez travailler, dans notre cas il faut choisir le fichier que vous venez de copier sur votre ordinateur.

![](../images/mod1_image_1.jpg)

 Vous aurez directement accès à tous les documents présent sur votre gitlab, une fois le fichier sélectionné vous pouvez le modifier directement depuis ATOM sans devoir vous connecter sur gitlab.

Vous pouvez ajouter du texte et des liens mais également des fichiers et des images, prenez grand soin de les compresser avant de les ajouter, vous pouvez utiliser GIMP (semblable à photoshop/ totalement gratuit) pour modifier la taille de vos images.

Télécharger le grâce au lien suivant :  [GIMP_download](https://www.gimp.org/downloads/)  

![](../images/mod1_image_2.jpg)

### Git add / Git commit / Git push

Une fois que vous avez vérifié votre travail il est temps de l’enregistrer vers votre site.   
Pour que toutes vos modifications soit prises en compte, vous pouvez effectuer les manipulations suivantes directement depuis ATOM ou vous rendre dans votre terminal à l'emplacement du fichier copier et d’introduire les commandes suivantes :

```
Git add - A
```
Cette commande va enregistrer tous les fichiers modifiés depuis votre ordinateur

```
Git commit – m « comment »
```
La commande qui suit permet d’envoyer ces nouvelles données vers le gitlab

```
Git push
```
Il s’agit de la dernière commande utilisée pour publier des changements locaux sur le gitlab


Pour suivre l'état de vos dossiers n'hésitez pas à vérifier que tous vos documents sont à jour grâce à la commandes suivante :

```
leade@DESKTOP-4M0TKVI MINGW64 ~/lea.debauche (main)
$ git status
On branch main
Your branch is up to date with 'origin/main'.

nothing to commit, working tree clean
```
Vous êtes désormais prêt à documenter vos propres travaux  !
