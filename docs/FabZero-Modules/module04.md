# Module 4. Découpe assistée par ordinateur

Ce module est divisé en deux grandes parties:

- la première partie vous permettra de vous familiariser avec la marche à suivre et le coté technique de la découpeuse laser, en créant une bande test des différentes puissances de découpe de la machine.
cette première étape est importante et va vous aider à **déterminer les paramètres de la découpeuse laser** par rapport au matériau à découper.

- la deuxième partie sera plus créative, je vous présenterai les différentes étapes de **réalisation d'une lampe** avec seulement une source lumineuse et une feuille de polypropylène (50x80) sur base des paramètres identifiés au préalable.

[lampe_SVG](../images/lampe archi design finale.svg)

## Informations importantes

Au fablab nous avons accès à deux machines de découpe laser :

**LASERCUTTER - EPILOG**

Il d'agit d'une machine avec une surface de travail de 50x80cm et un laser d'une puissance de 40 watt.

**LASERCUTTER - LASERSAUR**

cette découpeuse laser à été conçue par le fablab, ses dimensions sont les suivantes:   
122x61 cm sur 12 cm de hauteur avec une puissance de 100 Watt.

Ces 2 machines fonctionnent principalement avec le format de fichier SVG, pour plus de facilité nous travailleront donc sur le programme inkscape que vous pouvez télécharger ci-dessous.

[Inkscape_download](https://inkscape.org/ja/release/inkscape-1.1.1/).

Pour utiliser les machines du fablab, il est également nécessaire de procéder à une réservation par crénaux horaire sur [Fabman](https://fabman.io/login?unknownInvitation=true).

Vous aurez accès à toutes les machines du fablab, à leur état d'utilistaion en cours et aux horaires de réservation, il vous permettra également d'activer la machine en scannant le QR code de celle-ci une fois sur place.

**Matériaux**

|   Recommandés    | Déconseillés | Interdits |
|------------------|--------------|-----------|
|bois contreplaqué |     MDF      |  Téflon   |
|    carton        |   ABS, PS    |  le PVC   |
|    papier        |  les métaux  | le cuivre |
|   acrylique      |  polyétylène | le vinyle |
|certains textiles |  composites  |simili-cuir|
|                  |   de fibre   |   Résine  |

**Risques**

Les lasers utilisent des faisceaux infrarouges très puissant, premièrement ces faisceaux risquent de mettre le feu à certains matériaux, mais il risquent également d'entrainer des dégats irréverssible à l’œil. Pour se protéger de ces risques il est conseillé de porter des lunettes de protection, de ne pas quitté la pièce pendant la découpe et d’avoir des extincteurs à portée de mains.

## Partie 1 : se familiariser avec les paramètres de la découpeuse laser

Ce premier excercie sera réalisé sur la découpeuse laser LASERSAUR

### Dessin vectoriel

Dans un premier tant, nous allons tracer dans inkscape l'élément que nous souhaitons découper, si vous utilisez une autre logiciel pensez à verctoriser chaques lignes tracées (automatique dans insckape/ logiciel vectoriel).

Nous avons réalisé par groupe une bandelette sur laquelle nous allons effectuer une série de découpe avec une puissance constante (10Watt) mais en variant la vitesse pour chaque trait de 100 à 2300.

![](../images/mod4_image1.jpg)

L'objectif est de tester la résistance au pliage de chaque trait de découpe. Ce test nous permettra de gagner du temps pour choisir les paramètres de découpes de notre future lampe.


### Paramètres de découpe

 Pour la suite, enregistrez votre fichier SVG sur une clé usb et ouvrez le fichier sur l'interfaste de la découpeuse laser LASERSAUR (logo dinosaure).

![](../images/mod4_image2.jpg)

**Problème rencontré**

Une fois le document terminé et importé dans le programme de découpe laser, il nous était impossible de modifier les paramètres de découpe de chaque trait.

**Solution**

Pour pouvoir découper les traits du dessin, le programme de découpe laser  associe chaque trait à une couleur, et ensuite chaque couleur à une vitesse et une puissance de découpe. Pour modifier ces paramètres il est nécessaire que tous les traits avec les mêmes caractéristiques souhaitées possèdent la même couleur (attention le programme ne reconnait pas le noir comme une couleur). Il faut donc corriger le document SVG sur inkscape.

![](../images/mod4_image3.jpg)

Une fois le problème réglé on définit les différents paramètres de chaque trait: on laisse la puissance fixe de 10Watt et on modifie uniquement  la vitesse, on effectue des variations allant de 100 à 2300.

![](../images/mod4_image4.jpg)

Pensez également à bien ajuster la position de la feuille et du dessin à découper avant de confirmer l'opération.

### Découpe

Tous les paramètres sont prêt, il est temps de préparer la machine pour la découpe.

Placez le matériau à découper à l'intérieur de la machine, dans ce cas ci une feuille en polypropylène de 50x80 cm (dans l'idéal utilisez une feuille de récupération si possible).Pour que la feuille ne bouge pas pendant la découpe on ajoute des éléments plus lourd aux extrémités de celle-ci.

![](../images/mod4_image5.jpg)

Fermez le couvercle de la machine et ouvrez la vanne d'absorption d'air.

![](../images/mod4_image6.jpg)

Vous pouvez lancer l'opération ! une fois la découpe terminée attendez quelques minutes que les fumées soit aspirées.

voilà le résultat !

![](../images/mod4_image7.jpg)

## Partie 2 : Réalisation d'une lampe

Pour la réalisation de ma lampe, le lien à mon objet me tient particulièrement à coeur, je souhaite donc réaliser une lampe qui puisse se ranger et se transporter à l'intérieur de mon tube à dessin.

Dans cette même logique j'ai choisi une source lumineuse sans fil de petite taille.
Cette source lumineuse produit une lumière très dirigée, principalement utilisée pour se fixer à l'intérieur des gardes robes par exemple.

![](../images/mod4_image10.jpg)

Au sujet de la lumière j'aimerais créer une lampe qui permette différents renduS de lumière en fonction du lieu et des besoins. une lampe modulable qui puisse se régler en fonction de la lumière désirée par son utilisateur, permettant ainsi une lumière plus intense et très dirigée ou une lumière douce et diffuse.

![](../images/mod4_image8.jpg)
![](../images/mod4_image9.jpg)

### Recherche

Différents test en papier, ensemble de cylindre emboités les uns dans les autres pour permettre d'allonger et de rétracter la lampe.

![](../images/mod4_image11.jpg)

Après les différents test, échec, aller-retour, j'ai finalement réalisé un modèle à la main dans la matière finale de la lampe pour tester ce matériaux et l'effet qu'il donne à la lumière.

J'ai été surprise du résultat, la lampe modulable permettait de produire différents types de lumière comme je l'espérais.

![](../images/mod4_image12.jpg)

### Réalisation

Cet excercie sera réalisé sur la découpeuse laser EPILOG.

Pour la réalisation finale de la lampe, il est nécessaire de tracer le gabarit de tous les éléments de celle-ci, pour ce faire j'ai choisi de travailler sur le logiciel Autocad.

Pour plus de facilité j'ai commencé par tracer un rectangle de 50x80 cm, dimension de la feuille sur laquelle je vais venir découper le gabarit de ma lampe.

Je viens ensuite tracer 5 bandes qui seront les futurs cylindres emboités autour de la source lumineuse.

Pour pouvoir fixer la lampe dans le tube je réalise également un disque dans lequel les attaches de la source lumineuse viendront s'emboiter.

J'applique une couleur pour chaque type de découpe :

- trait rouge = trait de découpe exterieur
- trait jaune = trait de découpe intérieur
- trait bleu = trait de pliage

![](../images/mod4_image13.jpg)

N'oubliez pas il est nécessaire d'avoir un fichier SVG pour pouvoir découper votre dessin. Pour vous aider voici un lien vers un convertisseur DWG to SVG.

[Convertisseur](https://cloudconvert.com/dwg-to-svg).

Enregistrez votre fichier sur une clé usb et installer vous à la machine de découpe laser que vous avez réservez. Ouvrez votre fichier avec inkscape et vérifiez une dernière fois l'échelle, toutes les couleurs de traits et les épaisseurs pour ne pas avoir de mauvaises surprises à l'impression.

![](../images/mod4_image14.jpg)

Une fois que vous êtes prêt cliquez dans fichier, imprimer, et votre dessin s'ouvrira dans le logiciel de découpe laser.

il est temps de définir pour chaque trait (couleur) une puissance et une vitesse de découpe pour cela référez vous à vos test de découpe préalable (voir partie 1).

![](../images/mod4_image15.jpg)

Vos paramètres sont encodés, placez votre matériau à l'intérieur de la machine, n'oubliez pas d'allumer l'extracteur d'air situé à proximité et ensuite lancez la découpe.

sur cette découpeuse laser pas besoin d'attendre pour ouvrir la machine apres la fin de l'impression il n'y a pas de fumées.

l'impression est termminée voici le résultat final une fois la lampe assemblée !

![](../images/mod4_image16.jpg)
