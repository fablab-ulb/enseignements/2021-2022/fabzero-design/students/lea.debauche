# Projet final

Après avoir suivi l’ensemble des formations nécessaire il est temps de s’attaquer à la réalisation de notre projet final.

## Le tube à dessin

Cet objet doit pouvoir être réalisé avec les machines du fablab (première contrainte imposée).

L'objectif est de créer un tube extensible et adapté à tous les types de documents mais qui puisse également être réduit à son état minimum pour être ranger et transporter facilement lorsqu'il n'est pas utilisé (prototype 1).    

Le tube doit également fournir une protection des documents contre les chocs (prototype 2).

## Design or not design ?
Pour réaliser au mieux notre objet, Victor Levis a partagé avec nous une marche à suivre pour ne négliger aucune étape de la conception d’un objet design.

La première question qu’il nous a posé est : "qu’est-ce qu’un un objet design ?"

Un objet serait défini par **une fonction et un style**, un objet design peut posséder les deux ou être dépourvu de la fonction, nous illustrons se principe en prenant l’exemple de la caricature du mobilier dans « jacques Tati » on peut observer un canapé, on en connait la fonction mais il en est dépourvu car on ne parvient pas à s’y installer, cela reste malgré tout ou avant tout un objet design.

![](./images/projet_image1.jpg)

Dans les catégories d’objets Il existe donc des outils et des signes. A travers cet objet nous travaillerons principalement sur la question de l’outil, en suivant étape par étape le processus suivant :

1.	**Déconstruire la fonction de l’objet** : devenir un simple observateur, se poser des questions sur l’utilisation de l’objet, chercher à le comprendre, lui et ses problèmes.

2.	**Résoudre un vrai problème** : mise en place des limites et des contraintes, elles font l’objet.

3. **Comprendre les dimensions** : penser en ergonome, gestes et poids, chaque objet à des règles de dimensions, l’usage génère la forme. L’esthétique vient après, les dimensions accompagnent le dessin.

4.	**Esquisse essais, erreur, amélioration** :  prototype sale, prototype propre et prototype fonctionnel, chacun tente de résoudre les problèmes du prototype précédent pour arriver à un objet final idéal.

5.	**Adaptation, diffusion édition** : comment rendre l’objet diffusable pour le plus grand nombre, capable d’être reproduit avec les machines accessibles.

6.	**Un nom un pictogramme** : rendre l’objet compréhensible, le lier à un univers.


## 1. Déconstruction de la fonction de l’objet

Il s’agit d’un tube de transport personnel, il est de couleur noir mat, en matière solide, rigide et imperméable. Tout est en plastique sauf la lanière qui est en tissus, la texture extérieure du tube est rugueuse, pas lisse comme à l’intérieur.

4 parties : couvercle, tube coulissant, tube fixe et attaches.

-	**Couvercle** : élément à visser sur le tube, possède une partie transparente sur l'extérieure qui peut s’enlever et dans lequel on trouve une étiquette pour inscrire son nom ou le contenu du tube.

-	**Tube coulissant** :  le tube est donc réglable dans la longueur (son diamètre ne change pas), une partie est creusée dans la longueur du tube pour pouvoir emboiter des éléments qui servent à bloquer le tube dans une position (la longueur souhaitée), pour chaque position clé la dimension des feuilles de type A0, A1, ect, est inscrite. Les sens de rotation du tube, de déverrouillage et de blocage des positions, sont également inscrits. On remarque une fine bordure dans les encoches de blocage du tube, elles sont présentes pour ne pas que le tube se débloque tout seul. Cette partie du tube ne possède pas de fond ni de cran d’arrêt dans quand on l’étend au maximum les deux parties peuvent se détacher.

-	**Tube fixe** : son diamètre est légèrement plus grand pour pouvoir insérer la partie coulissante à l’intérieure. On observe la présence de non pas une, mais trois encoches pour venir bloquer la partie coulissante du tube. Le tube fixe possède une certaine longueur impossible à réduire donc en cas de transport de petits formats, ces-derniers restent parfois bloqués dans le fond du tube et deviennent très compliqué à récupérer.

-	**Attaches** : attaches sur lesquelles sont fixées les deux extrémités de la lanière de transport, celle du haut est fixée au tube impossible à détacher, l’attache du bas est coulissante le long du tube, pratique pour ajuster en fonction de la taille mais difficile à faire coulisser, pas de blocage au bout du tube, l’attache s’enlève si on la descend trop bas. La lanière en tissus est également réglable.

![](./images/projet_image2.jpg)

Position dans son usage mobile pratique à transporter au dos facile à régler. Pour le poser à l’arrêt, couché il tourne, debout il n’est pas stable, il tombe, on ne sait pas comment le ranger.

On peut transporter toutes les tailles de documents mais il n’est pas adapté pour les plus petits, écrasement de documents possible lorsqu’on veut réduire la dimension du tube (les feuilles se déroulent au maximum dans la partie du tube fixe et le tube coulissant vient écraser ces documents en longeant la parois intérieure).

Pour sortir les feuilles du tube c'est également un problème, car elles se déroulent à l'intérieur et sont difficile à sortir.

## 2. Résoudre un vrai problème

Après décomposition de cet objet on remarque que ça qualité principale engendre également ses plus gros défauts. Le coté extensible du tube est extrêmement pratique mais cause énormément de problèmes, beaucoup d'éléments sont présents pour remédier à ces problèmes mais au final ne suffisent toujours pas.   
Ne faudrait-il pas trouver un autre système de déploiement que celui de l'emboitement ?

## 3. Comprendre les dimensions

Il existe **2 catégories de tubes à dessins**, dans un premier temps les *tubes destinés à l'export* de document, ces tubes sont souvent en carton avec deux couvercles en plastique à chaque extrémité, ils ne sont pas réglables mais sont suffisamment solides pour protéger des chocs pendant le transport et sont disponibles en de nombreuses dimensions.

![](./images/projet_image3.jpg)

En ce qui nous concerne on s'intéresse d'avantage au type de *tube à dessin destiné à l'usage personnel*, ces tubes sont plus généralement en plastique (polypropylène dur) et sont disponibles dans des dimensions allant de 50 à 120 cm de longueur et variant entre 70 et 130 mm de diamètre.

Il existe des tubes à dessins télescopiques et d'autres tubes fixes, les premiers sont plus pratiques pour transporter des travaux de formats différents, malgré les problèmes rencontrés. Les plus efficaces possèdent un couvercle à visser plutôt qu'un couvercle simple car celui-ci risque de s'ouvrir pendant le transport ce qui nous rappelle que la présence de lanières pour le porter à l'épaule est un grand avantage.

![](./images/projet_image4.jpg)

**La longueur du tube** dépends de la taille de la feuille qu'on souhaite glisser à l'intérieur, c'est sur base de ces dimensions qu'on définira la taille maximum du tube.

![](./images/projet_image5.jpg)

Pour pouvoir accueillir tous les formats ci-dessus, le tube doit pouvoir s'étendre sur une longueur de 850 mm.

*le rapport au corps se situe au niveau de son transport au dos de l'utilisateur, un tube trop grand risque de gêner et d'encombrer les mouvements de déplacement de son propriétaire.*

**le diamètre du tube**, un diamètre trop large deviendra encombrant, mais un trop étroit posera problème pour insérer des documents roulés si le papier est plus épais. Après utilisation de mon propre tube à dessin 90 mm de diamètre me semble idéal pour pouvoir rouler beaucoup de documents aussi bien en papier fin qu'en papier plus épais.

*Le rapport au corps se situe au niveau de sa prise en main, un tube trop large risque de ne pas être facile à manipuler.*

## 4. Prototypes
### Prototype 1 : réduire le tube à sa taille minimum

A travers ce premier prototype je souhaite répondre aux problèmes liés à l'extension du tube en réalisant un objet en matière souple (tissus) pour qu'il puisse se replier et se ranger facilement.

![](./images/projet_image7.jpg)

Pour ce premier prototype j'ai décidé de réaliser un tube réduit pour un format maximum de feuilles A2

#### Partie 1 : système d'extension / réduction          
Pour rester dans un ensemble de matériaux souple et permettre cette modularité de l'objet j'ai choisi un système d'extension/réduction par **"enroulement"**.

Ce système permet de régler le tube à n'importes quelles dimensions intermédiaires entre son état minimum et sa dimension maximum.

Pour réaliser cet enroulement il est préférable d'avoir un tissus à la fois assez fin, fluide et résistant.

![](./images/projet_image8.jpg)
#### Partie 2 : système de fermeture
Pour refermer le tube je profite du système d'enroulement, je fixe un système d'attaches reliées à l'élastique qui entoure la partie supérieur du tube. Une fois roulé je viens emboiter ces attaches entre elles pour stabiliser la position du tube et refermer celui-ci.

![](./images/projet_image8'.jpg)

#### Partie 3 : système d'attaches et de transport
Ces attaches sont destinées à fermer le tube et également à maintenir la cordelière de transport.

L'attache située à l'extrémité haute du tube est décomposée en deux éléments, chacun fixé à l'élastique de part et d'autre du système d'ouverture du tube. Ces attaches vont s'emboiter entre elles pour maintenir le tube fermé. Une fois attachées elles formeront une boucle dans laquelle pourra glisser la cordelière de transport, ce système permet à la cordelière de coulisser et d'être réglée directement en fonction de la taille du tube.

![](./images/projet_image9.jpg)

Pour la partie basse l'attache vient également se fixer à l'élastique de la cordelière, cette fois fixée à l'attache et ne dois pas pouvoir s'en détacher.

![](./images/projet_image10.jpg)

#### Résultats

L'objectif recherché est atteint, le principe fonctionne, j'ai réussi à réduire le tube à un ensemble de 10 cm de long.

J'ai utilisé l'élastique destiné à tenir l'attache en bas du tube pour maintenir tous les éléments ensemble une fois repliés.

Pour l'instant tous ces éléments sont encore assemblés séparément, dans la suite du processus j'aimerais trouver un moyen plus rapide de monter et démonter l'objet.

![](./images/projet_image11.jpg)

#### Améliorations

Ce prototype rencontre un problème, il est très souple ce qui est favorable pour le système de rangement mais défavorable lors du transport au niveau de la protection des documents.

Il est nécessaire d'ajouter une nouvelle contrainte, pour que lors du transport les documents soient protégés des chocs.

## Recherches

#### Technologies
Pour protéger les documents la première idées qui m'est venue est de mettre la **toile du tube en tension**.
Voici différentes techniques utilisées dans d'autres domaines pour mettre une toile ou un tissus en tension:

**Gonflage**   
Technique utilisée dans les systèmes de fauteuils de plage, cette technique nécessite un ensemble parfaitement imperméable et étanche.

![](./images/projet_image12.jpg)

Avantages : pas besoin d'éléments supplémentaires à ajouter au dispositif qui viendrait alourdir ou encombrer le tube.

Inconvénients : gonflage à la bouche ou secouer, peu pratique, peu approprier au lieu d'utilisation du tube parfaitement étanche sinon le système ne fonctionne plus = très fragile.

**Tiges en tension**   
Technique beaucoup utilisée dans les systèmes de camping pour les tentes.

![](./images/projet_image13.jpg)

Avantages : mise en tension très forte du dispositif.

Inconvénients : induis une taille et une position fixe du tube, est difficile à réaliser au fablab.

**Cercles de serrage**   
Technique utilisée en broderie, permet de tendre la surface de tissus sur laquelle on désire broder

![](./images/projet_image14.jpg)


Avantages : utilisation simple, déjà adapté pour un système circulaire, peut être combiné avec les attaches du tubes.

Inconvénients : ajout de deux pièces circulaires dans le dispositif de rangement prend plus de place qu'au départ.


#### Matériaux
Une autre façon de protéger les documents est de créer une **doublure à l'intérieur du tube** et d'y insérer une couche de protection supplémentaire, cependant cette couche doit être assez fine et flexible pour permettre l'enroulement du tube

**Doublure mousse Néoprène**   
Couche supplémentaire donc couche de protection plus solide que le tissus absorbe les chocs.

### Prototype 2 : protéger les documents des chocs

**Essai n°1 : doublure néoprène**

J’ai choisi un matériau qui est à la fois rigide et souple, qui peut se découper à la machine laser, il possède une épaisseur de 2 mm, ni trop fine (aucun intérêt au niveau de la mise en tension) ni trop épaisse (risque de prendre trop de place).


![](./images/projet_image15.jpg)

Dans un premier temps je tente de doubler l’intérieur du tube sur toute sa longueur, le système semble bien fournir une couche de protection aux documents à l'intérieur et une fois totalement vide, il se replie facilement sans prendre beaucoup plus de place que le précédent.

![](./images/projet_image16.jpg)

Cependant lorsque j'essaye de régler le tube dans une position intermédiaire, la doublure se plie et risque donc d'abimer les documents.

![](./images/projet_image17.jpg)

On peut donc utiliser cette doublure mais par sur la partie réglable du tube.


Cette doublure en mousse est uniquement envisageable sur la partie basse du tube avec une longueur maximum correspondant à la plus petite position réglable = dimension A3 ; 300 mm


![](./images/projet_image18.jpg)

**Essai n°2 : cerclage**

J’ai adapté la technique observée en broderie à mon objet, en utilisant deux cercles rigide qui vont s'emboiter l'un dans l'autre à travers la couche de tissus du tube et ainsi tendre celui-ci.

L’un des cercle est placé à l'intérieur du tube et l’autre à l'extérieur, quand ces deux élément viennent s'emboiter, ils prennent avec eux la couche de tissu qui les sépare et mettent en tension celui-ci.

![](./images/projet_image19.jpg)

On peut y intégrer facilement l'attache pour faire passer la lanière de transport.

![](./images/projet_image20.jpg)

Cependant dans ce système on se retrouve avec un élément supplémentaire qui vient encombrer le système de rangement du tube.

![](./images/projet_image21.jpg)

## 5. Prototype fonctionnel
### Dimensions

Le tube peut accueillir des documents jusqu'à la dimension du format A1 (600 mm).

Il est réglable jusqu'à la taille d'une feuille A3 (réglable sur la partie souple en tissus et pas sur la partie en mousse de 300 mm).

Une fois vide il est totalement repliable sur lui-même.

![](./images/projet_image22.jpg)

### Matériaux
J'ai utilisé un **tissus très fin en polyester**, avant de lancer les découpe sur la laser et faut vérifier que le tissus est adapté à ce genre de découpe pour ce faire rendez-vous sur le site de green fabrick spécialisé dans ce domaine.

Sur ce blog vous trouverez les tests à réaliser sur vos échantillons de tissus avant la découpe et si ils sont conseillé ou non pour les machines laser.

![](./images/projet_image23.jpg)

[Green fabrick](https://greenfabric.be/blog-2/)

J'ai donc effectué une série de test sur le tissus, j'ai pu en déduire qu'il est nécessaire de découper à une grande vitesse et une faible puissance pour que le tissus ne fonde pas.

Résultat avec une vitesse trop faible
![](./images/projet_image24.jpg)
Résultat vitesse adaptée entre 70 et 95%
![](./images/projet_image25.jpg)

Le deuxième matériau utilisé est la **mousse néoprène**, après diverses recherches cette mousse de fine épaisseur est également adaptée à la découpe laser.

![](./images/projet_image26.jpg)

### Techniques et mise en oeuvre

Après les tests effectués sur les différents prototypes j'ai décidé d'exploiter la partie du fond du tube en mousse et la partie supérieur avec les cercles de serrage.

![](./images/projet_image27.jpg)

#### Partie 1 : tissus avec Doublure

![](./images/projet_image28.jpg)
#### Partie 2 : cercles de serrages

1. test avec une attache extérieure

2. test avec une attache plus fine (qui à cassé)

3. test avec deux attaches intégrée

![](./images/projet_image29.jpg)
#### Partie 3 : transport et rangement

La lanière de transport fixée dans le bas du tube et au niveau du cercle de serrage externe.

![](./images/projet_image30.jpg)
Replis de l'objet sur lui-même, peut ainsi être ranger facilement dans son sac.

![](./images/projet_image31.jpg)

### Utilisation au quotidien
Pour pouvoir proposer un produit final parfaitement utilisable j'ai décidé de l'utiliser dans mes démarches quotidiennes de façon à tester l'objet en condition réelle.

J'ai rassemblé les points d'analyses après chaque essai  de l'objet à chaque temps d'utilisation de celui-ci pour décrire précisément les points positifs ou négatifs rencontrés et à quel moment.

**Etape 1 : Insérer les documents dans le tube**    
Plutôt simple, se glisse parfaitement prend la forme de l'objet pas d'élément qui viennent gêner.

**Etape 2 : Fermer le tube**   
Très intuitif, enroulement simple, les attaches ont un bon maintien mais peut être un peu trop fragile pour l'utilisation à long terme.

**Etape 3 : Réglage avec cercle de serrage**   
Pour l'instant cercle dur à désserer, réglage et serrage simple mais la lanière ne coulisse pas facilement avec donc doit faire vas et viens pour regler, à long terme l'emboitement des cercles risque d'abimer le tissus.

**Etape 4 : Transport du tube plein**   
La lanière est trop présente, s'enmèle, difficile à régler, se détache sous le tube et abime le fond.
par contre une fois au dos très agréable, très léger, peut se tenir au dos ou à la main en fonction des situations.

**Etape 5 : Sortir les documents**   
Très facile et agréable, le tissus glisse facilement le long des feuilles très délicat n'abime rien, qu'elles soient attachées avec un élastique ou non, on peut retirer tout type de documents de toutes tailles. Seul point que j'améliorais est le bout l'ouverture plus large pour pouvoir passer ma main plus facilement pour atteindre l'extrémité des feuilles (simplifiera encore plus l'insertion des feuilles dans le tube également).

**Etape 6 : Replier et ranger le tube**   
Pas encore de façon précise de le replier, tous les éléments s'enroulent et se glisse dans le sac, je pense qu'il pourrait prendre encore moins de place mais que la présence des cercles m'en empêche, pas de moyen de le maintenir enrouler du coup se défait dans le sac et s'enmêle.

Après que le tube ai été rouler pour à nouveau mettre des documents à l'intérieur, pas de problème rencontrés, il reprends facilement sa forme de base.

**Etape 7 : Changer la housse ou la nettoyer en cas de salissure**   
Le tissus reste un élément fragile face aux conditions extérieures, sa composition finale sera impérméable mais Pour l'instant la conception du tube fait que la partie en tissu =housse fait partie intégrante du tube (le tube = la housse). Donc en cas de salissure je n'ai pas de moyen de discocier les éléments de structure pour pouvoir la laver ou la changer.

### Résultats

**Respecter au mieux ma première contrainte**   
Je dois réduire au minimum la taille de mes éléments solides dans l'objet, s'ils sont présent ils doivent être indispensables et ne pas créer plus de problèmes que de solutions.


Eléments à améliorer :
- système de cerclage complexe à utiliser rapidement, risque d'abimer le tissus à long terme, cercles prennent beaucoup de place à ranger, nécessitent beaucoup de lanières (une de chaque coté)

**Nouvelle contrainte**    
Suggérée par la matière, le système de housse en tissus imperméable doit pouvoir être séparée de la structure du tube pour laver et changer la housse en fonction des besoins (doit pouvoir s’enlever rapidement et aussi rentrer le système intérieur facilement)

Eléments à améliorer :
- Enroulage avec fixation
- Lanière moins encombrante
- Tissus fin et imperméable

## Recherches
### Technologies

Housse parapluie, sa fente permet l'insertion plus simple du parapluie dans la housse, applicable dans notre cas pour l'entrée et la sortie des documents du tube.   

![](./images/projet_image32.jpg)

Attache par enroulement avec élastique

![](./images/projet_image33.jpg)

### Matériaux
L'usage du tube à dessin nécessite un tissus qui protège de la pluie, il existe deux type de tissus qui propose ce genre de protection :

1. Les tissus déperlant :    
l'eau ruisselle le long du tissus mais en cas de forte intempérie l'eau fini par pénétré, risque d'humidité.

2. Les tissus imperméables :   
ils sont déperlant et sans risque de pénétration de l'eau, ils résistent au forte pluie sans aucun problème.

Pour une étanchéité optimale, je conseille également l'applicassion d'une colle spécifique, souvent utilisée dans la réparation des tentes, elle est à appliquer sur l'ensemble des coutures de l'objet. (voir application dans l'objet final)

#### Le Polyester   
Utilisé par exemple pour les nappes et tissus de mobiliers extérieurs.   

L'inconvénient majeur de ces tissus est qu'ils sont très épais et pour la plupart uniquement déperlant et donc pas complètement imperméable.

![](./images/projet_image34.jpg)

#### Le Nylon

Principalement utilisé pour les vêtements de pluie comme les vestes imperméables.

L'avantage de ce textile est qu'il est particulièrement fin et parfaiteemnt imperméable, il s'agit des deux qualités recherchées pour la réalisation de notre objet.

![](./images/projet_image35.jpg)

ideal = nylon imperméable (KW)
#### Le tissus avec mousse résille

Le néoprène est finalement peu adapté à long terme, il ne reprend plus sa forme.  

La mousse résille, permet une grande souplesse mais également un renfort considérable dans la protection du papier.

![](./images/projet_image36.jpg)


## 6. Nouveau prototype
### Prototype 3 : protection optimale des documents contre les chocs

Réflexion sur le système d'enroulement du papier, plus il est serré plus il est solide. Système qui permettrait l'accompagnement de cet enroulement avec une protection doublée en mousse résille.

![](./images/projet_image37.jpg)

#### Etape 1 : partie imperméable   

Sur la face extérieur je découpe une fente dans laquelle je viens insérer la pochette de rangement du tube, elle est également en matière imperméable de telle façon à ce qu'une fois replié il reste bien protégé.

![](./images/projet_image38.jpg)

#### Etape 2 : Doublure intérieure  

Pour fixer la doublure il suffit de retourner chaque face du tissus et de coudre le tout à environ 1 cm du bord, vos deux tissus sont à présent relié et vous pouvez retourner le tout dans le bon sens.

![](./images/projet_image39.jpg)

Dans ce prototype je propose un système de protection avec mousse résille, réparti en deux bandes qui peuvent se déplacer en fonction de la taille des documents à rouler.

![](./images/projet_image40.jpg)

#### Problèmes rencontrés  

Avec ce nouveau prototype la protection au choc est en effet bien solutionnée mais l'étanchéité complète de l'objet est extrêmement compliquée à atteindre et nécessite de nombreuses opérations de pliages et de fermetures à la main (étape supplémentaire dans la manipulation) ce qui devient extrêmement gênant dans l'utilisation de l'objet.    
De plus sa taille permettant d'accueillir un A0 est très compliquée à enrouler si on ne possède pas un grand espace pour étendre l'objet (grande table). Ce qui est rarement le cas chez un imprimeur quand on récupère nos documents par exemple.


## 7. Objet final

Pour concevoir cet objet final j'ai décidé de combiner deux prototype avec chacun leur qualités spécifiques.

Il s'agit du prototype fonctionnel, qui comprenait principalement un système de sac enroulable pouvant être totalement étanche et le prototype 3 sur ce concept de protection des chocs par bande de serrage enroulée autour du papier.

Le tube à dessin final sera dont un sac étanche munit de bandes de serrage anti-choc, avec un sac de rangement intégré.

#### Gabarit et dimensions

![](./images/projet_image41.jpg)
![](./images/projet_image42.jpg)


#### Fichiers

- Découpe laser   

[Test 1_SVG](../images/TEST 1 ligne de découpe.svg)   


[Test 2_SVG](../images/TEST 2 carré de découpe.svg)

[Découpe face KW_SVG](../images/découpe face extérieur tube tissus KW.svg)

[Découpe doublure_SVG](../images/découpe face intérieure tube tissus doublure.svg)

[Découpe bande et sac tissus KW_SVG](../images/découpe bandes et sac tissus KW.svg)

[Découpe bande mousse résille_SVG](../images/découpe bandes tissus mousse résille.svg)


- Impression 3D

[Boutons_STL](../images/boutons V1 .stl)   

[Attaches lanière_STL](../images/attaches lanière v2.stl)   

[Attaches fermeture_STL](../images/attache fermeture boucle v4.stl)

[Réglage lanière_STL](../images/reglage lanière v2.stl)

#### Matériaux

| Qty |  Description    |  Price  |           Link           | Notes  |
|-----|-----------------|---------|--------------------------|--------|
| 1 m | Bande tissus 1 mètre Doublure en polyester - Noir | 5,5 €/m| https://www.veritas.be/fr_be/doublure-en-polyester-au-metre-462101000002-z00#867=31
|  1 m  | Bande tissus 1 mètre imperméable KW en Nylon - Orange |  9 €/m |https://www.chienvert.com/fr/coupe-vent-fin/18722-tissu-coupe-vent-impermeable-orange.html |        |
| 50 cm  |Mousse résille double - Noir |  19,50 € / m| https://www.chienvert.com/fr/uni/30203-mousse-resille-double-noir.html  |        |
| 2   | Bande Velcro pour textile 19mmx60cm  |  3 €| https://www.veritas.be/fr_be/bande-velcro-pour-textile-19mmx60cm-463262ve0003-z00   |        |
| 1   | Bande élastique tissée - Noir  |  4 €/m| https://www.veritas.be/fr_be/elastisch-geweven-kousenband-46e100210027-za1#867=3061 |        |
| 1   | Bande tressée grise 20 mm  |  4 € / m| https://www.brico.be/fr/construction/mat%C3%A9riaux-de-construction/adh%C3%A9sifs-colles/colles-de-montage-et-accessoires/colle-%C3%A0-bois-pattex-pu-construct-60gr/5272347   |        |
| 1   | boutons pression à coudre en laiton argenté 9 mm  |  2,5 € / 12 pc| https://www.brico.be/fr/construction/mat%C3%A9riaux-de-construction/adh%C3%A9sifs-colles/colles-de-montage-et-accessoires/colle-%C3%A0-bois-pattex-pu-construct-60gr/5272347   |        |
| 1   | GEARAID Sealant + Adhesive (colle waterproof pour les coutures étanche)  |  8,5 € /2x 7g| magasin Lecompte - Rue de Vergnies 27 - 1050 Ixelles  |        |


#### Découpe laser
Avant toute chose il est nécessaire de vérifier si votre tissus convient à une découpe laser, n'oublier pas de vous référer au blog suivant en cas de doute :    [Green fabrick](https://greenfabric.be/blog-2/)   

Pour l'ensemble des découpe j'ai utilisé la machine de type **EPILOG** laser, pour rappel de l'utilisation de la machine référez vous au **"module 4. découpe assistée par ordinateur"**.

AVANTAGES :

La découpe des tissus au laser permet une très grande précision au niveau de l'horizontalité de la pièce, de plus le laser permet aux bords d'être légèrement cicatrisé par la chaleur et ainsi le tissu ne file pas lorsqu’il est manipulé.

- TEST 1 : ligne simple à travers les différents Matériaux

Malgré tous les test préalable j'effectue une première découpe simple et rapide à travers chacun des matériaux pour voir un éventuel défaut de celui-ci : par exemple fumée ou flamme

Dans ce cas-ci rien d'anormal n'est observé je peux donc continuer à utiliser la machine sans risques.

![](./images/projet_image43.jpg)

- TEST 2 : Carrés multiples
 Ce test est effectué pour connaitre la puissance et la vitesse idéale de découpe pour mes matériaux.

J’effectue ce test  en faisant varier la vitesse entre 80 et 100% et la puissance de 10 à 30 %.

je répète l'oppération sur un échantillon de chaque tissus.

![](./images/projet_image44.jpg)

- Découpe des faces intérieure et extérieure du tube

Ces éléments sont particulièrement grands, la machine que j'utilise ne couvre pas une aussi grande surface mais il est tout à fait possible de découper chaque partie en deux fois.

Pour ce faire le fichier a été préparer pour pouvoir activer uniquement la moitié de l'élément à découper.

![](./images/projet_image45.jpg)

Une fois dans le programme de découpe laser, sélectionnez uniquement les couleurs que vous souhaitez découper et placer les autres calques en OFF.

![](./images/projet_image46.jpg)

Ensuite choisissez de découper les éléments en couleur selon la vitesse et la puissance qui vous convienne, référez-vous à votre TEST 2 préalablement effectué. Je choisi une vitesse de 100% et une puissance de 30%.

Je place mon tissus bien à l'horizontal dans la machine et je replie délicatement le surplus de tissus de manière à ce qu'il ne gêne pas la découpe.

J'allume l'extracteur d'air et je peux lancer la découpe.

![](./images/projet_image47.jpg)


Une fois la première partie de la découpe effectuée je retourne ma pièce de tissus toujours en veillant à ce que le surplus ne gêne pas la découpe.

Pour une précision optimale de la découpe utiliser l'outil vidéo du programme de découpe situé en haut à gauche :
-	Pan
-	Vidéo
-	Ensuite retournez dans edit pour déplacer votre gabarit    

Cela vous permettra de voir l'emplacement exacte de votre découpe par rapport à la position de votre tissus dans la machine, faites correspondre ces lignes avec celles déjà découpées et ensuite relancez la découpe.


![](./images/projet_image48.jpg)


Si un léger décalage persiste il vous suffit de couper la jointure des deux découpes aux ciseaux.

Attention si ce message d’erreur apparait « right interlock open » il faut appuyer fermement sur la fermeture de la machine sinon la découpe ne se lancera pas .

Cette marche à suivre est identique pour les deux faces (intérieure et extérieure) du tube.

![](./images/projet_image49.jpg)

- découpes des bande antichocs

Pour ces plus petites pièces, une seule découpe suffit, imprimez un fichier à la fois (un matériau à la fois), les fichiers sont prévus pour deux bandes antichocs, selon vos préférences vous pourrez en réaliser autant que vous le souhaitez.
Pour les caractéristiques de puissance et de vitesse référez vous à votre test des carrés pour une découpe optimale.


![](./images/projet_image50.jpg)

#### Réalisation
##### Etape 1 : Sac tube étanche   

Munissez-vous des deux premières pièces en tissus KW et sa doublure. Ces pièces seront les parties intérieur et extérieur du tube il faut donc les travailler à l’envers pour éviter d’avoir des couture trop visible.    

Replier le bord extrême haut du tube sur une largeur de 3,5 cm et le bord du bard sur une largeur de 1 cm.    

Pratiquer une couture le long de du bord haut seulement
Ensuite plier le tissus en deux dans la longueur et toujours à l’envers, effectuer une couture le long tu tube sauf sur la zone pliée au niveau de l’ouverture du tube, à environ 10 mm du bord.    

![](./images/projet_image51.jpg)

Ensuite découper un fond de tube de forme circulaire pour chaque pièce et pratiquer une couture le long du bord bas laissé en suspens (travaillez toujours avec vos tissus envers visible).   

![](./images/projet_image52.jpg)

Pour un résultat étanche optimal ajouter de la colle waterproof le long de vos couture à l'intérieur du tube sur la partie en tissus imperméable KW.

![](./images/projet_image60.jpg)


##### Etape 2 : Bandes antichocs   

Réalisation très simple, travail toujours à l'envers, pliez le tissus en deux. Effectuez une couture le long du coté gauche et dans le fond de la bande à une distance d'environ un centimètre du bord.

Ensuite il vous suffit de retourner le tout, de fixer les trois boutons et l'attache élastique avant de coudre le dernier coté de la bande.

![](./images/projet_image62.jpg)

##### Etape 3 : Sac de rangement intégré   

premièrement coudre deux bord au petites extrémité du tissus d'environ 1 cm.

Replier le tissus en deux et coudre toute les extrémités ensemble sauf la partie avec les bord qui sera donc l'ouverture du sac.

Ensuite viendra la partie la plus délicate ou il faut fixer le sac entre la partie doublure du tube et la partie imperméable, effectuer une couture à la fois en commencent par fixer le sac à la doublure et ensuire à la couche imperméable.

![](./images/projet_image61.jpg)

#### Impression 3D
Pour que mon objet prenne le moins de place possible j'ai décidé de réaliser les plus petites pièces en 3D, il s'agit principalement des diverses attaches présente sur le tubes.

Pour la réalisation 3D des pièces suivantes, dans le logiciel prusa, il est nécessaire d'ajouter une bordure d'environ 5 mm, pour que les pièces restent bien collées à la plaque lors de l'impression.

Chaque impression dure entre 15 et 30 minutes pour un résultat optimal et le plus rapide possible.

- fermeture du tube :
[Attaches fermeture boucle](https://a360.co/35kdVfD)

Ces attaches sont conçues pour être facilement utilisée à la façon d'un clips. Les deux pièces sont fixées à une bande élastique située au niveau de l'ouverture du tube. Elles s'emboitent l'une dans l'autre pour permettre une ouverture simple et une fermeture solide.

![](./images/projet_image53.jpg)

- Fixation de la lanière de transport : [attaches lanière](https://a360.co/3fXaCgm)

Ces deux éléments identique se retrouve aux extrémités du tube. Ils sont insérés dans une bande élastique fixée au sac étanche du tube. Ces fixations vont permettre de maintenir la lanière de transport en place.

![](./images/projet_image54.jpg)

- Réglage de la lanière : [Réglage lanière](https://a360.co/3fZXcjU)

Cet élément est également réduit à son plus simple usage et est à ajusté à la dimension de votre lanière de transport (dans notre cas une ouverture de 20 mm est nécessaire).

![](./images/projet_image55.jpg)

- Boutons de serrage pour les bandes antichocs : [Boutons](https://a360.co/3u0ZzuY)

Pour éviter d'acheter une boite de boutons, réalisez les vous-même, avec ces éléments vous pourrez fixer vos bande de serrage comme il se doit.

![](./images/projet_image56.jpg)

#### Objet final
![](./images/projet_image57.jpg)

![](./images/projet_image59.jpg)

![](./images/projet_image58.jpg)
